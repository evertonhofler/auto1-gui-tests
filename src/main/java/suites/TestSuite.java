package suites;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import core.BaseSuite;
import tests.filterByYearAndSort;

@RunWith(Suite.class)
@Suite.SuiteClasses({
	filterByYearAndSort.class,
})

public class TestSuite extends BaseSuite {
	
}