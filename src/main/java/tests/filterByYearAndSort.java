package tests;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;

import core.BaseTest;
import core.Properties;
import support.Utilities;

public class filterByYearAndSort extends BaseTest {

    @Test
    public void filterByYearAndSort() throws IOException {
    	
    	String rawTotalResults;
    	long totalResults;
    	int searchResultNumOfPages;
    	int remainingSearchResults;
    	int pageSizesComboNumOfItems;
    	int pageSizesComboIndex;
    	int pageSize;
    	String searchYear = "2015";
    	int itemYear;
    	long price = 0;
    	long previousPrice = 0;
    	String gridModeLocator = "";
    	
    	searchPage.clickFilterYear();
    	searchPage.selectYear(searchYear);
    	searchPage.waitSearchExecution();

    	searchPage.sort("Höchster Preis");
    	searchPage.waitSearchExecution();
    	
    	rawTotalResults = searchPage.getTotalResults();
    	totalResults = Long.parseLong(rawTotalResults.substring(0, rawTotalResults.length() - 8));
    	
    	pageSizesComboNumOfItems = searchPage.getPageSizesComboSize();
    	
    	for (int viewMode = 0; viewMode < 2; viewMode++) {
    		
    		pageSizesComboIndex = 0;

    		if (viewMode == 1) {
    			gridModeLocator = searchPage.getGridModeLocator();
    			searchPage.clickGridMode();
    			searchPage.selectPageSize(pageSizesComboIndex);
    		}
    		
    		while (pageSizesComboIndex < pageSizesComboNumOfItems) {
    			
		    	pageSize = searchPage.getPageSize();
		    	searchResultNumOfPages = (int) (totalResults / pageSize);
		    	remainingSearchResults = (int) (totalResults % pageSize);
		    	
		        Utilities.takeScreenshot(Utilities.getCurrentLocalDateTimeStamp() + "_" + 
		        		Properties.BROWSER + "_" + testName.getMethodName());
		        
		    	while (searchResultNumOfPages >= 0 && pageSize > 0) {		    		
		    		
		    		if (searchResultNumOfPages == 0) {
		    			pageSize = remainingSearchResults;
		    		}
		    		
			    	Assert.assertEquals("Erstzulassung von: 2015", searchPage.getFilteredBy());
			    	for (int i = 1; i <= pageSize ; i++) {
			    		try {
			    			itemYear = searchPage.getItemYear(gridModeLocator, i);
		    			} catch(org.openqa.selenium.StaleElementReferenceException ex) {
			    			itemYear = searchPage.getItemYear(gridModeLocator, i);
		    				
		    			}
			    		Assert.assertTrue(itemYear >= Integer.parseInt(searchYear));
			    	}
			    	
			    	Assert.assertEquals("Höchster Preis", searchPage.getSortedBy()); 
			    	for (int i = 1; i <= pageSize ; i++) {
			    		try {
			    			price = searchPage.getItemPrice(gridModeLocator, i);
			    		} catch(org.openqa.selenium.StaleElementReferenceException ex) {
			    			price = searchPage.getItemPrice(gridModeLocator, i);
			    		}
			    		if (i > 1) {
			    			Assert.assertTrue(price <= previousPrice);
			    		}
			    		previousPrice = price;
			    	}
		
		    		if (searchResultNumOfPages == 0) {
		    			pageSize = 0;
		    		}
		    		else {
		    			searchResultNumOfPages = searchResultNumOfPages - 1;
		       			searchPage.clickNextPage();
		    		}
		    		
		    	}
		    	
		    	pageSizesComboIndex = pageSizesComboIndex + 1;
		    	if (pageSizesComboIndex < pageSizesComboNumOfItems) {
		    		searchPage.selectPageSize(pageSizesComboIndex);
		    	}
	    	}
	    }
    }
}