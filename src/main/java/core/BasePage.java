package core;

import static core.DriverFactory.getDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import support.Utilities;

public class BasePage {
	
    public void selectComboItemByIndex(By by, int index) {
    	WebElement element = getDriver().findElement(by);
    	Select combo = new Select (element);
    	combo.selectByIndex(index);
    }

    public void selectComboItemByText(By by, String text) {
    	WebElement element = getDriver().findElement(by);
    	Select combo = new Select (element);
    	combo.selectByVisibleText(text);
    }

    public String getComboSelectedItem(By by) {
    	WebElement element = getDriver().findElement(by);
    	Select combo = new Select (element);
    	return combo.getFirstSelectedOption().getText();
    }

    public int getComboSize(By by) {
    	WebElement element = getDriver().findElement(by);
    	Select combo = new Select (element);
    	return combo.getOptions().size();
    }
    
    public void type(String idElement, String text) {

        getDriver().findElement(By.id(idElement)).sendKeys(text);
    }

    public void click(String idElement) {

        getDriver().findElement(By.id(idElement)).click();
    }

    public void click(By by) {

        getDriver().findElement(by).click();
    }

    public String getText(By by) {
        return getDriver().findElement(by).getText().trim();
    }

    public String getUrl() {
        return getDriver().getCurrentUrl();
    }

    public void waitTextToBePresent(By by, String text) {
    	WebDriverWait wait = new WebDriverWait(getDriver(), 5);
    	wait.until((ExpectedCondition<Boolean>) driver -> getDriver().findElement(by).getAttribute("textContent").equals(text));
    }
   
    public void waitTextToContain(By by, String text) {
    	WebDriverWait wait = new WebDriverWait(getDriver(), 5);
    	wait.until((ExpectedCondition<Boolean>) driver -> getDriver().findElement(by).getAttribute("textContent").contains(text));
    }

}