package core;

import static core.DriverFactory.getDriver;
import static core.DriverFactory.killDriver;

import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.TestName;

import pages.SearchPage;
import support.JUnitHTMLReporter;

public class BaseTest extends JUnitHTMLReporter {
	
	protected SearchPage searchPage = new SearchPage();
	
    @Before
    public void initializeTest() {

    	getDriver().get(Properties.getURL());
    }

    @Rule
    public TestName testName = new TestName();

    @After
    public void finalizeTest() throws IOException {
        if (Properties.CLOSE_BROWSER) {
            killDriver();
        }
    }

}