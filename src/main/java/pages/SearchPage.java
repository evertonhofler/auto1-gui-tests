package pages;

import static core.DriverFactory.getDriver;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import core.BasePage;
import core.Properties;

public class SearchPage extends BasePage {
	
	public SearchPage waitSearchExecution() {
		waitTextToContain(By.cssSelector("div.root___26IzG > div.resultsAmount___3OrV7"), "Ldt...");
		waitTextToContain(By.cssSelector("div.root___26IzG > div.resultsAmount___3OrV7"), "Treffer");
		return new SearchPage();
	}
	
	public SearchPage selectPageSize(int index) {
		selectComboItemByIndex(By.name("pageSize"), index);
		return new SearchPage();
	}
	
	public SearchPage clickGridMode() {
		click(By.cssSelector("div.root___26IzG > div.viewMode___1yRpC.hidden-xs > label:nth-child(1) > svg"));
		return new SearchPage();
	}

    public SearchPage clickFilterYear() {
        click(By.cssSelector("div:nth-child(3) > div.label___3agdr > span"));
        return new SearchPage();
    }
    
    public SearchPage clickNextPage() {
    	WebElement paginationUL = getDriver().findElement(By.cssSelector("div:nth-child(3) > div > div.text-center > div.hidden-xs > div > ul"));
    	List<WebElement> pageList = paginationUL.findElements(By.tagName("li"));
    	for (WebElement li : pageList) {
    		if (li.getText().equals("›")) {
        		List<WebElement> aList = li.findElements(By.tagName("a"));
        		for (WebElement a : aList) {
        			try {
        				a.click();
        			} catch(org.openqa.selenium.StaleElementReferenceException ex) {
        				a.click();
        			}
        		}
    		}
    	}
        return new SearchPage();
    }
    
    public SearchPage selectYear(String text) {
    	selectComboItemByText(By.name("yearRange.min"), text);
        return new SearchPage();
    }
    
    public int getPageSizesComboSize() {
    	return getComboSize(By.name("pageSize"));
    }
    
    public SearchPage sort(String mode) { 
    	selectComboItemByText(By.name("sort"), mode);
        return new SearchPage();
    }
    
    public String getTotalResults() {
    	return getText(By.cssSelector(("div.root___26IzG > div.resultsAmount___3OrV7")));
    }
    
    public String getFilteredBy() {
        return getText(By.cssSelector("li:nth-child(1) > button"));
    }
    
    public int getPageSize() {
    	return Integer.parseInt(getComboSelectedItem(By.name("pageSize")));
    }

    public String getSortedBy() {
        return getComboSelectedItem(By.name("sort"));
    }
    
    public int getItemYear(String modeLocator, int index) {
    	return Integer.parseInt(getText(By.cssSelector("div:nth-child(3) > div > div.root___2j9X2" + modeLocator 
    			+ " > div:nth-child(" + index + ") > div > a > ul > li:nth-child(1)")).substring(5));
    }

    public Long getItemPrice(String modeLocator, int index) {
    	String rawItemPrice;
    	rawItemPrice = getText(By.cssSelector("div > div.root___2j9X2" + modeLocator 
    			+ " > div:nth-child(" + index + ") > div > a > div.price___1A8DG > div.totalPrice___3yfNv"));
    	return Long.parseLong(rawItemPrice.substring(0, rawItemPrice.length() - 2).replace(".", ""));
    }
    
    public String getGridModeLocator() { 
    	return ".grid___2iZPr";
    }
}